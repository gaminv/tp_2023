// 2023-04-09 alexeit at home 12:40 TP 2023, ios manipulators
// https://flylib.com/books/en/2.131.1/writing_your_own_stream_manipulators.html
#ifndef IOMANIP_HPP
#define IOMANIP_HPP
#include <ios>
#include <iomanip>

// make floating-point output look normal
inline std::ios_base& floatnormal(std::ios_base& io) {
 io.setf(~std::ios_base::floatfield, std::ios_base::floatfield);
 return(io);
}


// from IAS stream, custom manipulator

// структура для примера пользовательского оператора вывода
struct Price
{
  int price;    // цена товара в копейках
};

// header guard для сохранения состояния потока
class StreamGuard
{
public:
  explicit StreamGuard(std::basic_ios<char> &s) :
    s_(s),
    precision_(s.precision()),
    flags_(s.flags())
  {}
  ~StreamGuard() {
    s_.precision(precision_);
    s_.flags(flags_);
  }
private:
  std::basic_ios<char> &s_;
  std::streamsize precision_;
  std::basic_ios<char>::fmtflags flags_;
};

//-----оператор вывода с манипуляторами exact и fractional-------------------------------------------------------------
// std::ios_base::xalloc() возвращает уникальный индекс
static const int PRICE_FORMAT_INDEX = std::ios_base::xalloc();
static const int EXACT_PRICE_FORMAT = 1;
static const int FRACTIONAL_PRICE_FORMAT = 0;

// манипулятор для вывода в виде копеек
template<class Char, class CharTraits>
std::basic_ostream<Char, CharTraits> & exact(std::basic_ostream<Char, CharTraits> &stream)
{
 // устанавливаем значение по индексу
 stream.iword(PRICE_FORMAT_INDEX) = EXACT_PRICE_FORMAT;
 return stream;
}

// манипулятор для вывода в дробном виде
template<class Char, class CharTraits>
std::basic_ostream<Char, CharTraits> & fractional(std::basic_ostream<Char, CharTraits> &stream)
{
 stream.iword(PRICE_FORMAT_INDEX) = FRACTIONAL_PRICE_FORMAT;
 return  stream;
}

std::ostream &operator<<(std::ostream &stream, const Price &price)
{
 std::ostream::sentry sentry(stream);
 if (sentry)
 {
   StreamGuard guard(stream);
   // читаем значение по индексу
   if (stream.iword(PRICE_FORMAT_INDEX) == EXACT_PRICE_FORMAT)
   {
     stream << price.price;
   }
   else
   {
     stream << std::setprecision(2) << std::fixed
       << static_cast<double>(price.price) / 100;
   }
 }
 return stream;
}

#endif
