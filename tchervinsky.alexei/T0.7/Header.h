#include <iostream>
#include <string>
#include <map>
#include <exception>

//����������� ������//
template <typename T>
class Dic_list
{

private:
	template<typename U>
	struct Node
	{
		Node* pNext;
		T data;

		Node(T data = T(), Node* pNext = nullptr)
		{
			this->data = data;
			this->pNext = pNext;
		}
	};
	int Size;
	Node<int>* head; //��� �������� ������ ����� ���������� � ������������ ������

public:
	Dic_list();
	~Dic_list();

	void push_back(T data); // �������� ������� � ����� ������
	void pop_front(); // ������ ������ ������ ������
	int GetSize() { return Size; }
	void insert(T data);
	void clear_dict();
	T& operator[](const int index); //����� ���������� ��������
	void push_front(T data); // ���������� �������� � ������ ������
	void insert(int value, int position); // ������� ������ �������� � ������
	void removeAt(int index);
	void pop_back();

	bool search(Node<T>* head, int index) // �������� � ������� 
	{
		Node<T>* current = head;
		while (current != nullptr)
		{
			if (current->data == index)
			{
				return true;
			}
			current = current->pNext;
		}
		return false;
	}

	void merge(Dic_list<T>& list2)
	{
		Node<T>* position = this->head;
		while (position->pNext != nullptr)
		{
			position = position->pNext;
		}
		position->pNext = list2.head;
	}

	//Dic_list<T> getInterSection(map<string, int>& dict1, map<string, int>& dict2)
	//{
	//	for (auto& p : dict1) // ����������� �� ������� �������
	//	{
	//		auto it = dict2.find(p.first); // ���������, ���� �� ����� ����� �� ������ �������
	//		if (it != dict2.end())// ���� ����� ���� �� ������ �������
	//		{
	//			result[p.first] = p.second + it->second; // ��������� ����� � ����� ������� � ������ �������� �� ����� ��������
	//		}
	//			
	//	}
	//	return result; // ���������� ����� �������
	//}
};

template<typename T>
Dic_list<T>::Dic_list()
{
	Size = 0; //�������� ��������� ��������� 
	head = nullptr; // ��������� �� ��, ��� ��� ������� ����
}

template<typename T>
Dic_list<T>::~Dic_list()
{
	clear_dict();
}

template<typename T>
void Dic_list<T>::push_back(T data)
{
	if (head == nullptr)
	{
		head = new Node<T>(data);
	}
	else
	{
		Node<T>* current = this->head;

		while (current->pNext != nullptr) //�� ���������� �� ���� ����� � ������� ���������
		{
			current = current->pNext;
		}
		current->pNext = new Node<T>(data);
	}
	Size++;
}

template<typename T>
 void Dic_list<T>::pop_front() // ����� �� ������� �������, �� ��������� ���������� size--
{
	Node<T>* temp = head;

	head = head->pNext;

	delete temp;

	Size--;
}

 template<typename T>
 void Dic_list<T>::clear_dict()
 {
	 while (Size)
	 {
		 pop_front();
	 }
 }

 template<typename T>
T& Dic_list<T>::operator[](const int index)
{
	if (index >= Size)
	{
		throw std::runtime_error("incorrect index in [] operator");
	}
	int counter = 0;
	Node<T>* current = this->head;
	while(current!=nullptr)
	{
		if (counter == index)
		{
			return current->data;
		}
		current = current->pNext;
		counter++;
	}
	return head->data;
}

template<typename T>
void Dic_list<T>::push_front(T data)
{
	head = new Node<T>(data, head); // ������� ����� ���� �� ��� ������ ������ ������ � �������� ����� �������
	Size++;
}

template<typename T>
void Dic_list<T>::insert(int value, int position) // ������ ������ ��������
{
	// ������� ����� ���� � �������� ���������
	Node<T>* newNode = new Node<T>(value);

	// ���� ������� ����� 0, �� �� ��������� ����� ���� � ������ ������ � ������ ��� ����� �������
	if (position == 0) 
	{
		newNode->pNext = head;
		head = newNode;
	}
	else 
	{
		// ����� �� ������� ���� �� ������� `position - 1`
		Node<T>* previousNode = head;
		for (int i = 0; i < position - 1; i++) 
		{
			previousNode = previousNode->pNext;
		}
		// ��������� ����� ���� ����� ���������� ����
		newNode->pNext = previousNode->pNext;
		previousNode->pNext = newNode;
	}
	// ����������� ������ ������
	Size++;
}

template<typename T>
inline void Dic_list<T>::removeAt(int index)
{
	if (index == 0)
	{
		pop_front();
	}
	else
	{
		Node<T>* previous = this->head;
		for (int i = 0; i < index - 1; i++)
		{
			previous = previous->pNext;
		}
		Node<T>* toDelete = previous->pNext;
		previous->pNext = toDelete->pNext;
		delete toDelete;
		Size--;
	}
}

template<typename T>
inline void Dic_list<T>::pop_back()
{
	removeAt(Size - 1);
}





















	//Node<T>* new Node = new Node(data); // ������ ����� ������� � ������ 
	//if (head == nullptr)
	//{
	//	head = new Node;
	//}
	//else if (value < head->new Node)
	//{
	//	new Node->pNext = head;
	//	head = new Node;
	//}
	//else
	//{
	//	while (current->pNext != nullptr && current->pNext->data < value) 
	//	{
	//		current = current->pNext;
	//	}

	//	new Node->pNext = current->pNext;
	//	current->pNext = new Node;
	//}

